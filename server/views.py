import asyncio
import logging
import traceback
from asyncio import sleep, gather, CancelledError
from datetime import datetime

from aiohttp import WSMsgType
from aiohttp.web import View, RouteTableDef
from aiohttp.web_response import Response
from aiohttp.web_ws import WebSocketResponse

from server.resources import Resource

routes = RouteTableDef()


@routes.view('/api/resources')
class ResourceView(View):
    async def get(self):
        loop = asyncio.get_event_loop()
        ws = WebSocketResponse()
        sending = True

        time_interval = float(self.request.query.get('interval', 5))
        resources = self.request.query.getall('resource', ['cpu'])

        assert time_interval > 0
        assert all(res in Resource.resources for res in resources)

        async def send_stats():
            nonlocal sending
            while sending:
                started = datetime.now()
                try:
                    await ws.send_json(
                        await loop.run_in_executor(None, Resource.get_values, resources)
                    )
                except Exception:
                    logging.warning(traceback.format_exc())
                    sending = False

                await sleep(
                    max(0.0, time_interval - (datetime.now() - started).total_seconds())
                )

        async def wait_for_close():
            nonlocal sending
            try:
                async for msg in ws:
                    if msg.type == WSMsgType.TEXT:
                        if msg.data == 'close':
                            break
                    elif msg.type in (WSMsgType.ERROR, WSMsgType.CLOSED):
                        break
            except CancelledError:
                pass

            sending = False
            await ws.close()

        await ws.prepare(self.request)

        try:
            await gather(send_stats(), wait_for_close())
        except Exception:
            logging.warning(traceback.format_exc())

        return ws
