from aiohttp import web

from server.views import routes

app = web.Application()

app.add_routes(routes)

app.router.resources()
